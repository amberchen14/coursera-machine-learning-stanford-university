function [C, sigma] = dataset3Params(X, y, Xval, yval)
%DATASET3PARAMS returns your choice of C and sigma for Part 3 of the exercise
%where you select the optimal (C, sigma) learning parameters to use for SVM
%with RBF kernel
%   [C, sigma] = DATASET3PARAMS(X, y, Xval, yval) returns your choice of C and 
%   sigma. You should complete this function to return the optimal C and 
%   sigma based on a cross-validation set.
%

% You need to return the following variables correctly.
C = 1;
sigma = 0.3;

% ====================== YOUR CODE HERE ======================
% Instructions: Fill in this function to return the optimal C and sigma
%               learning parameters found using the cross validation set.
%               You can use svmPredict to predict the labels on the cross
%               validation set. For example, 
%                   predictions = svmPredict(model, Xval);
%               will return the predictions on the cross validation set.
%
%  Note: You can compute the prediction error using 
%        mean(double(predictions ~= yval))
%
val=[0.01; 0.03; 0.1; 0.3; 1; 3; 10; 30];
pre_y=zeros(size(yval));
pre_error=zeros(size(C,1), 3);
a=1;
for i = 1: size(val,1)
  for j  = 1: size(val,1)
     c=val(i);
     sigma=val(j);
     model= svmTrain(X, y, c, @(x1, x2) gaussianKernel(x1, x2, sigma));
     pre_y=svmPredict(model, Xval);
     pre_error(a, 1)=c;
     pre_error(a, 2)=sigma;
     pre_error(a,3)=mean(double(pre_y ~= yval));
     a=a+1;
  endfor
endfor

index=find(pre_error(:, 3)==min(pre_error(:, 3)));
C=pre_error(index, 1);
sigma=pre_error(index, 2);







% =========================================================================

end
